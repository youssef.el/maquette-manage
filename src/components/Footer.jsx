import React from 'react'
import logo from '../assets/images/logo.svg'
import fb from '../assets/images/icon-facebook.svg'
import twitter from '../assets/images/icon-twitter.svg'
import instagram from '../assets/images/icon-instagram.svg'
import pin from '../assets/images/icon-pinterest.svg'
import yt from '../assets/images/icon-youtube.svg'

const Footer = () => {
  return (
    <div className='text-center flex flex-col px-6 py-10 bg-blue-950 lg:flex-row-reverse'>
        <div className='flex flex-col justify-between'>
            <div className="flex space-x-2 mx-auto">
                <input type="text" placeholder='Updates in your inbox...' className='text-sm py-2 pl-6 pr-16 rounded-full ' />
                <button className='rounded-full bg-orange-500 px-6 drop-shadow-xl hover:bg-orange-400' >GO</button>  
            </div>
            <p className='hidden mx-auto text-sm pt-10 text-gray-500 md:inline-block'>Copyright 2020. All Rights Reserved</p>
        </div>

        <div className="grid grid-cols-2 py-10 text-white text-left mx-auto space-x-10">
            <div className="flex flex-col space-y-2">
                <a href="" className='cursor-pointer hover:text-orange-500 '>Home</a>
                <a href=""className='cursor-pointer hover:text-orange-500 '>Pricing</a>
                <a href=""className='cursor-pointer hover:text-orange-500 '>Products</a>
                <a href=""className='cursor-pointer hover:text-orange-500 '>About Us</a>
            </div>
            <div className="flex flex-col space-y-2">
                <a href=""className='cursor-pointer hover:text-orange-500 '>Careers</a>
                <a href=""className='cursor-pointer hover:text-orange-500 '>Community</a>
                <a href=""className='cursor-pointer hover:text-orange-500 '>Privacy Policy</a>
            </div>
        </div> 
        
        <div className="flex flex-col lg:flex-col-reverse lg:justify-between">
            <div className="flex items-center justify-around space-x-8 mx-auto pb-10 md:w-1/2 lg:w-3/4">
                <img src={fb} alt="" width='40px' className='cursor-pointer hover:bg-orange-500 '/>
                <img src={yt} alt="" width='40px' className='cursor-pointer hover:bg-orange-500'/>
                <img src={twitter} alt="" width='40px' className='cursor-pointer hover:bg-orange-500'/>
                <img src={pin} alt="" width='40px' className='cursor-pointer hover:bg-orange-500'/>
                <img src={instagram} alt="" width='40px' className='cursor-pointer hover:bg-orange-500'/>
                
            </div>
            <img src={logo} alt=""  width='140px' className='self-center' />
            
        </div>

        <p className='block mx-auto text-sm pt-10 text-gray-500 md:hidden'>Copyright 2020. All Rights Reserved</p>
        
      
    </div>
  )
}

export default Footer
