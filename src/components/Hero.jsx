import React from 'react'
import img from '../assets/images/illustration-intro.svg'

const Hero = () => {
  return (
    <div className='container  flex flex-col-reverse items-center justify-between mx-auto p-6 sm:flex-row'>
      <div className="flex flex-col max-w-md space-y-8 sm:space-y-12 ">
        <h2 className='text-4xl text-center font-bold md:text-5xl md:text-left max-w-md '>Bring everyone together to build better products.</h2>
        <p className='text-lg text-center md:text-left text-gray-500'>Manage makes it simple for software teams to plan day-to-day tasks while keeping the larger team goals in view.</p>
        <button className='block text-lg self-center bg-orange-500 text-white font-semibold  py-2 px-6 rounded-full drop-shadow-xl hover:bg-orange-400 w-1/2 md:self-start'>Get Started</button>
      </div>
      <div >
        <img src={img} alt="" />
      </div>
    </div>
  )
}

export default Hero
