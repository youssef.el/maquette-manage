import React, { useState } from 'react'
import ali from '../assets/images/avatar-ali.png'
import anisha from '../assets/images/avatar-anisha.png'
import richard from '../assets/images/avatar-richard.png'
import shanai from '../assets/images/avatar-shanai.png'
import ClientCard from './ClientCard'
import { FaRegCircle , FaCircle ,FaChevronLeft ,FaChevronRight } from "react-icons/fa";

const Clients = () => {

  const slides= [{
                img:ali,
                name:'ali bravo',
                text:'Lorem ipsum dolor sit amet consectetur adipisicing elit.Labore consectetur ad quia at maxime quisquam?'},
                {img:anisha,
                name:'anisha brama',
                text:'Lorem ipsum dolor sit amet consectetur adipisicing elit.Labore consectetur ad quia at maxime quisquam?'},
                {img:richard,
                name:'richard watt',
                text:'Lorem ipsum dolor sit amet consectetur adipisicing elit.Labore consectetur ad quia at maxime quisquam?'},
                {img:shanai,
                name:'shanai brdly',
                text:'Lorem ipsum dolor sit amet consectetur adipisicing elit.Labore consectetur ad quia at maxime quisquam?'}  
              ]

  const [current, setCurrent] = useState(0);

  const next = () => {
    const nextSlide = current < (slides.length-1) ? current + 1 : 0
    setCurrent(nextSlide);
  }
  
  const prev = () => {
    const prevSlide = current > 0 ? current-1  : slides.length-1  
    setCurrent(prevSlide)
  }
  

  return (
    <div className='flex flex-col items-center text-center mx-auto justify-center py-6 space-y-16'>
      <h2 className='px-12 w-5/6 py-10 mx-auto sm:w-full font-bold text-3xl sm:4xl md:5xl '>What they've said</h2>
      {/* slider */}
      <div className='grid'> 
        <div className="relative group ">
          <ClientCard 
                  img={slides[current].img}
                  name={slides[current].name}
                  text={slides[current].text}
          />
          {/* slider arrows */}
          <div className='hidden group-hover:block absolute translate-x-0 top-[50%] translate-y-[-50%] left-8 bg-black/20 rounded-full p-2 hover:bg-white/80' > 
              <FaChevronLeft onClick={prev} size={20} />
          </div>
          <div className='hidden group-hover:block absolute translate-x-0 top-[50%] translate-y-[-50%] right-8 bg-black/20 rounded-full p-2 hover:bg-white/80' > 
              <FaChevronRight onClick={next} size={20} />
          </div>
        </div>
        {/* slider dots */}
        <div className="flex mx-auto pt-4">
          {slides.map((slide,indexOfSlide)=>(
              <div key={indexOfSlide} onClick={()=> setCurrent(indexOfSlide)} className="rounded-full pl-2">
                {indexOfSlide == current ? <FaCircle size={12} /> : <FaRegCircle size={12}/>}
              </div>
          ))}
        </div>
      </div>
      <button className='text-lg place-self-center bg-orange-500 text-white font-semibold  py-2 px-6 rounded-full drop-shadow-xl hover:bg-orange-400 w-1/2 md:w-1/4'>Get Started</button>
    </div>
    
  )
}

export default Clients
