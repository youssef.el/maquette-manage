import React from 'react'

const FeaturesCard = (props) => {
  return (
    <div className="flex flex-col pt-6 space-y-3">
            <div className="flex item-center justify-center relative" >
                <button className='bg-orange-600 ml-6 z-10 text-xl rounded-full text-center font-bold text-white py-2 px-6 mr-[-2.3rem]'>
                    {props.number}
                </button >
                <h4 className='py-2 font-bold bg-pink-100 text-xl w-full md:pl-14 md:bg-transparent'>
                    {props.title}
                </h4>
            </div>
            <p className='text-md  mx-auto text-left px-6 text-gray-500  md:pl-28 '>
                {props.text}
            </p>
        </div>
  )
}

export default FeaturesCard
