import React from 'react'

const CallToAction = () => {
  return (
    <div className='bg-orange-400 text-center flex flex-col space-y-10 py-20  md:flex-row md:justify-evenly md:items-center md:py-20'>
      <h1 className='text-5xl font-bold  mt-10 px-12 leading-normal text-white md:mt-0 md:text-left md:w-1/2 md:text-4xl'>Simplify how your team works today.</h1>
      <button className='text-lg w-1/2 self-center bg-white text-orange-400 font-semibold  py-2 px-6 rounded-full drop-shadow-xl hover:bg-orange-100 md:w-1/6 '>Get Started</button>
    </div>
  )
}

export default CallToAction
