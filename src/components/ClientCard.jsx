import React from 'react'

const ClientCard = (props) => {
  return (
    <div className="bg-gray-200 flex flex-col items-center text-center space-y-4 pb-6 px-12 mx-6">
        <img src={props.img} width='100px' className='mt-[-3rem]'/>
        <h6 className='text-md font-semibold'>{props.name}</h6>
        <p>{props.text}</p>
      </div>
  )
}

export default ClientCard
