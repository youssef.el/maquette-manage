import React from 'react'
import FeaturesCard from './FeaturesCard'

const Features = () => {
  return (

      <div className="container grid  mx-auto md:grid-cols-2 py-6 text-center sm:text-left md:px-6">
        
        <div className="flex  flex-col space-y-4 md:space-y-8">
            <h2 className='w-1/2 pt-10 mx-auto sm:w-full font-bold text-3xl sm:4xl md:5xl md:pt-4 '>What's different about Manage?</h2>
            <p className='text-md  text-center  mx-auto px-6  text-gray-500 md:w-3/4  md:ml-0 md:text-left md:px-0' >Lorem ipsum dolor sit amet consectetur 
                adipisicing elit.Quaerat omnis maiores, 
                labore quae consequuntur placeat.</p>
        </div>
        
        <div >
        <FeaturesCard 
                    number='01' 
                    title='Track company-wide progress'
                    text='See how your day-to-day tasks fit into the wider vision. Go from tracking progress
                     at the milestone level all the way done to the smallest of details.
                     Never lose sight of the bigger picture again. ' />
         <FeaturesCard 
                    number='02' 
                    title='Advanced built-in reports'
                    text='Set internal delivery estimates and track progress toward company goals.
                     Our customisable dashboard helps you build out the 
                    reports you need to keep key stakeholders informed.' />
         <FeaturesCard 
                    number='03' 
                    title=' Everything you need in one place'
                    text='Stop jumping from one service to another to communicate, store files, track tasks
                     and share documents. Manage offers an all-in-one team productivity solution. ' />

        </div>
        
      </div>
      
  )
}

export default Features

