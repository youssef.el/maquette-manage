import React, { useState } from 'react'
import logo from '../assets/images/logo.svg'
import Hamburger from '../assets/images/icon-hamburger.svg'
import Close from '../assets/images/icon-close.svg'

const Header = () => {

  const [open ,setOpen] = useState(false)

  function handleClick(){
    setOpen(prev => !prev)
  }

  return (
    <div className="relative container mx-auto p-6">
        <div className='flex items-center justify-between'>
            <img src={logo} alt="logo" />
            <div className="hidden  text-sm font-medium text-gray-600 space-x-6 md:flex">
                <a href="#" className=' hover:text-orange-500'>Pricing</a>
                <a href="#" className=' hover:text-orange-500'>Product</a>
                <a href="#" className=' hover:text-orange-500'>About Us</a>
                <a href="#" className=' hover:text-orange-500'>Careers</a>
                <a href="#" className=' hover:text-orange-500'>Community</a>
            </div>
            <button className='hidden text-sm bg-orange-500 text-white font-semibold  py-2 px-6 rounded-full drop-shadow-xl hover:bg-orange-400 md:block'>Get Started</button>
            {/* small screen humberger menu  */}
            <button onClick={handleClick} className='pr-1 md:hidden'><img src={open ? Close : Hamburger} alt="" /></button>
        </div>
        {/* collapse menu for small screens */}
        {open && <div className="flex flex-col text-sm text-center font-medium text-gray-600 space-y-6 mx-auto pt-6 md:hidden transform ease-in duration-700">
                <a href="#" className=' hover:text-orange-500'>Pricing</a>
                <a href="#" className=' hover:text-orange-500'>Product</a>
                <a href="#" className=' hover:text-orange-500'>About Us</a>
                <a href="#" className=' hover:text-orange-500'>Careers</a>
                <a href="#" className=' hover:text-orange-500'>Community</a>
        </div>}
    </div>
  )
}

export default Header
